package main

import (
	"flag"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/kendellfab/podcast"
)

func main() {

	var portFlag string
	flag.StringVar(&portFlag, "p", ":8080", "Set the port to bind the http listener to.")
	flag.Parse()

	db, err := gorm.Open("sqlite3", "podcast.db")
	defer db.Close()

	if err != nil {
		log.Fatal("error opening database", err)
	}

	episodeManager := podcast.NewEpisodeManager(db, "http://localhost:8080")

	log.Println("Episode Manager", episodeManager)

}
