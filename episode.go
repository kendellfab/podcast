package podcast

import (
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Episode struct {
	ID          string     `json:"id" gorm:"type:varchar(48)"`
	CratedAt    time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `json:",omitempty" sql:"index"`
	Title       string     `json:"title"`
	Slug        string     `json:"slug"`
	ShowID      string     `json:"showId"`
	OwnerID     string     `json:"ownerId"`
	Link        string     `json:"link"`
	Description string     `json:"description"`
	Notes       string     `json:"notes"`
	PubDate     time.Time  `json:"pubDate"`
	ImageURL    string     `json:"imageUrl"`
	AudioURL    string     `json:"audioUrl"`
	AudioLength int64      `json:"audioLength"`
	AudioType   string     `json:"audioType"`
	OggURL      string     `json:"oggUrl"`
	Active      bool       `json:"active"`
	Explicit    string     `json:"explicit"`
	Duration    int64      `json:"duration"`
}

// BeforeCreate a gorm callback that generates a UUID for this episode
func (e *Episode) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("ID", id.String())
}

// BeforeSave a gorm callback that turns a title into lowercase and replaces all spaces with a dash
func (e *Episode) BeforeSave(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(e.Title, " ", "-", -1))
	e.Slug = slug
	return nil
}
