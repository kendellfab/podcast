package podcast

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

var ErrorShowIDRequired = errors.New("show id required")

type EpisodeManager struct {
	orm     *gorm.DB
	baseURL string
}

func NewEpisodeManager(orm *gorm.DB, baseURL string) EpisodeManager {
	orm.AutoMigrate(&Episode{})
	return EpisodeManager{orm: orm, baseURL: baseURL}
}

func (em EpisodeManager) Store(r io.Reader, userID string) (Episode, error) {
	var episode Episode
	err := json.NewDecoder(r).Decode(&episode)
	if err != nil {
		return episode, err
	}

	if episode.ShowID == "" {
		return episode, ErrorShowIDRequired
	}

	episode.OwnerID = userID
	err = em.orm.Create(&episode).Error
	if err != nil {
		return episode, fmt.Errorf("error creating episode: %w", err)
	}
	episode.Link = fmt.Sprintf("%s/episode/%s", em.baseURL, episode.Slug)

	err = em.orm.Model(&episode).Update("link", episode.Link).Error
	if err != nil {
		return episode, fmt.Errorf("error updating link: %w", err)
	}

	return episode, nil
}

func (em EpisodeManager) Get(id string) (Episode, error) {
	var episode Episode
	err := em.orm.Where("ID = ?", id).First(&episode).Error
	return episode, err
}
