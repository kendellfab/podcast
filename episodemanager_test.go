package podcast

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func TestStoreEpisode(t *testing.T) {
	em, err := inMemoryEpisodeManager()
	if err != nil {
		t.Fatal("in memory database error", err)
	}

	episodeInput := Episode{Title: "First Episode", Description: "The first episode.", ShowID: "my-show"}
	episodeBytes, err := json.Marshal(episodeInput)
	if err != nil {
		t.Fatal("error generating input bytes", err)
	}

	episodeBuf := bytes.NewBuffer(episodeBytes)

	episode, err := em.Store(episodeBuf, "ABC123")
	if err != nil {
		t.Fatal("error storing episode", err)
	}

	if episode.ID == "" {
		t.Fatal("error expected ID to be set")
	}

	if episode.Slug != "first-episode" {
		t.Fatalf("error expected slug to be %s but got %s", "first-episode", episode.Slug)
	}

	if episode.Link == "" {
		t.Fatal("error expected link to be set")
	}

	if episode.ShowID != "my-show" {
		t.Fatalf("error incorrect show id expected %s but got %s", "my-show", episode.ShowID)
	}

	if episode.OwnerID == "" {
		t.Fatal("error ownerid should be set to ABC123")
	}

	fromDB, err := em.Get(episode.ID)
	if err != nil {
		t.Fatal("error loading episode with ID")
	}

	if fromDB.Title != "First Episode" {
		t.Fatalf("error expected title %s but got %s", "First Episode", fromDB.Title)
	}
}

func TestEmptyShowID(t *testing.T) {
	em, err := inMemoryEpisodeManager()
	if err != nil {
		t.Fatal("in memory database error", err)
	}

	episodeInput := Episode{Title: "First Episode", Description: "The first episode."}
	episodeBytes, err := json.Marshal(episodeInput)
	if err != nil {
		t.Fatal("error generating input bytes", err)
	}

	episodeBuf := bytes.NewBuffer(episodeBytes)

	_, err = em.Store(episodeBuf, "ABC123")
	if err == nil {
		t.Fatalf("expected %s but error is nil", ErrorShowIDRequired.Error())
	}
}

func TestBadInput(t *testing.T) {
	em, err := inMemoryEpisodeManager()
	if err != nil {
		t.Fatal("in memory database error", err)
	}

	episodeBuf := bytes.NewBufferString("not-valid-json")
	_, err = em.Store(episodeBuf, "ABC123")
	if err == nil {
		t.Fatal("error should have invalid json")
	}
}

func inMemoryEpisodeManager() (EpisodeManager, error) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		return EpisodeManager{}, err
	}

	return NewEpisodeManager(db, "http://localhost:8080"), nil
}
