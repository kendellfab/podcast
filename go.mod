module gitlab.com/kendellfab/podcast

go 1.13

require (
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/jinzhu/gorm v1.9.11
	github.com/pkg/errors v0.8.0
)
